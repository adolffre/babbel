//
//  Words.m
//  Babbel
//
//  Created by A. J. on 03/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Words.h"

static int const numberOfWords = 4;
static NSString const *wordInEnglish = @"text_eng";
static NSString const *wordInSpanish = @"text_spa";

@interface Words()

#pragma mark - Propertys

@property (nonatomic, strong) NSMutableArray *allWords;
@property (nonatomic, strong) NSDictionary *wordDictionary;
@property (nonatomic, strong) NSMutableArray *fallingWords;
@property (nonatomic, strong) NSMutableArray *passedWords;
@property (nonatomic, strong) NSString *fallingWord;

@end

@implementation Words

#pragma mark - Init

-(instancetype)initWords{
    
    self = [super init];
    if(self)
    {
        NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"words" ofType:@"json"];
        NSData *data = [NSData dataWithContentsOfFile:jsonPath]; NSError *error = nil;
        self.allWords = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        self.passedWords = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Custom Methods

-(void)createNewFallingWords{
    
    NSMutableArray *words = [NSMutableArray new];
    float countAllWords = [self.allWords count];
    while (words.count != numberOfWords) {
        NSDictionary *wordDictionary =  self.allWords[arc4random_uniform(countAllWords)];
        NSString *word = [wordDictionary objectForKey:wordInEnglish];
        if([self isDiferentFromEnglishWord:word]){
            [words addObject:[wordDictionary objectForKey:wordInSpanish]];
        }
    }
    [words addObject:[self getSpanishWord]];
    self.fallingWords = words;
    [self.passedWords removeAllObjects];
}

-(BOOL)isDiferentFromEnglishWord:(NSString *)word{
    
    return ![[self getEnglishWord] isEqualToString:word];
}

-(void)createNewWord{
    
    float countAllWords = [self.allWords count];
    NSDictionary *wordDictionary =  self.allWords[arc4random_uniform(countAllWords)];
    self.wordDictionary = wordDictionary;
    [self createNewFallingWords];
}

-(NSString *)getNextFallingWord{
    
    [self verifyFallingWords];
    float count = self.fallingWords.count;
    NSString *nextWord = self.fallingWords[arc4random_uniform(count)];
    [self.fallingWords removeObject:nextWord];
    [self.passedWords addObject:nextWord];
    return nextWord;
}

-(void)verifyFallingWords{
    
    if(self.fallingWords.count == 0){
        self.fallingWords = [NSMutableArray arrayWithArray:self.passedWords];
        [self.passedWords removeAllObjects];
    }
}

#pragma mark - Getters

-(NSString *)getEnglishWord{
    
    return [self.wordDictionary objectForKey:wordInEnglish];
}

-(NSString *)getSpanishWord{
    
    return [self.wordDictionary objectForKey:wordInSpanish];
}

@end
