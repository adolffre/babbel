//
//  Words.h
//  Babbel
//
//  Created by A. J. on 03/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Words : NSObject

-(instancetype)initWords;

-(void)createNewWord;
-(NSString *)getSpanishWord;
-(NSString *)getEnglishWord;
-(NSString *)getNextFallingWord;

@end
