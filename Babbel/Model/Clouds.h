//
//  Clouds.h
//  Babbel
//
//  Created by A. J. on 03/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Cloud.h"

@interface Clouds : NSObject

@property (nonatomic , strong) Cloud *cloud;
@property (nonatomic , strong) UIImageView *cloudImageView;

-(instancetype)initWithSuperView:(UIView *)superView;

-(void)showCloudWithWord:(NSString *)word;
-(void)stop;
-(void)fastWord;
-(void)wrongWordPressed;
-(void)correctWordPressed;

@end
