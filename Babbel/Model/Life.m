//
//  Life.m
//  Babbel
//
//  Created by A. J. on 04/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Life.h"

@interface Life()

#pragma mark - Propertys

@property(nonatomic) NSInteger indexLife;

@end

@implementation Life

#pragma mark - Init

-(instancetype)initWithHearts:(NSArray<Heart *> *)hearts{
    
    self = [super init];
    if(self)
    {
        self.hearts = hearts;
        self.indexLife = 2;
        for (Heart *heart in self.hearts) {
            heart.alive = YES;
        }
    }
    return self;
}

#pragma mark - Custom Methods

-(void)removeLife{
    
    [(Heart *)self.hearts[self.indexLife] setAlive:NO];
    self.indexLife--;
}

-(BOOL)hasLife{
    
    return self.indexLife>=0 ? YES : NO;
}

@end
