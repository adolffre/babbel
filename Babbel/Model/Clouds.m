//
//  Clouds.m
//  Babbel
//
//  Created by A. J. on 03/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Clouds.h"

static int const hightSmallLabel = 31;
static int const widthSmallLabel = 80;
static int const startYCloud = -100;
static int const fastGravity = 2;
static int const fontSizeCloud = 22;
static float const slowGravity = 0.05f;

@interface Clouds()

#pragma mark - Propertys

@property (nonatomic, strong) UIView *superView;
@property (nonatomic, strong) UIDynamicAnimator* animator;
@property (nonatomic, strong) UIGravityBehavior* gravity;

@end

@implementation Clouds

#pragma mark - Init

-(instancetype)initWithSuperView:(UIView *)superView{
    
    self = [super init];
    if(self)
    {
        self.superView = superView;
    }
    return self;
}

#pragma mark - Custom Methods

-(void)showCloudWithWord:(NSString *)word{
    
    if(self.cloud)
        [self stop];
    self.cloud = [[Cloud alloc] initCloudWithSmallSize:[self isSmallCloud:word] andWord:word];
    [self cloudConfiguration];
}

-(void)cloudConfiguration{

    self.cloud.cloudView.center = CGPointMake(self.superView.frame.size.width / 2, startYCloud);
    [self.superView addSubview:self.cloud.cloudView];
    
    UITapGestureRecognizer *cloudPressed = [[UITapGestureRecognizer alloc] initWithTarget:self action:(@selector(cloudPressed:))];
    
    [self.cloud.cloudView addObserver:self forKeyPath:@"center" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.cloud.cloudView addGestureRecognizer:cloudPressed];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.superView];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.cloud.cloudView]];
    self.gravity.magnitude = slowGravity;
    
    [self.animator addBehavior:self.gravity];

}

-(void)stop{
    
    [self.cloud.cloudView removeObserver:self forKeyPath:@"center"];
    [self.cloud.cloudView removeFromSuperview];
    self.cloud = nil;
    self.animator = nil;
    self.gravity = nil;
}

-(void)fastWord{
    
    self.gravity.magnitude = fastGravity;
}

-(void)wrongWordPressed{
    
    [self.cloud changeToRed];
}

-(void)correctWordPressed{
    
    [self.cloud changeToGreen];
}

- (BOOL)isSmallCloud:(NSString *)word{
    
    NSAttributedString *attr = [self atributedStringFromWord:word];
    CGRect rect = [attr boundingRectWithSize:CGSizeMake(widthSmallLabel, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine context:nil];
    return hightSmallLabel == CGRectGetHeight(CGRectIntegral(rect));
}

-(UIFont *)wordTextFont {
    
    return [UIFont fontWithName:@"AvenirNext-Bold" size:fontSizeCloud];
}

#pragma mark - ObserveValue

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
    
    CGFloat hightView = [[UIScreen mainScreen] bounds].size.height;
    UIView *view = object;
    
    if(view.frame.origin.y >= hightView){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cloudOutOfView" object:self];
    }
}

#pragma mark - Actions
-(void)cloudPressed:(UITapGestureRecognizer *)tap{
    
    NSDictionary* userInfo = @{@"word": self.cloud.word};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"cloudPressed" object:self userInfo:userInfo];
}

#pragma mark - Calculate Size of Label

-(NSAttributedString *)atributedStringFromWord:(NSString *)word{
    
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:word attributes:@{
                                                                                                      NSForegroundColorAttributeName : [UIColor blackColor],
                                                                                                      NSFontAttributeName :[self wordTextFont]
                                                                                                      }];
    return attributedText;
}

@end
