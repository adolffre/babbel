//
//  Life.h
//  Babbel
//
//  Created by A. J. on 04/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Heart.h"

@interface Life : NSObject

@property(nonatomic, strong) NSArray<Heart*> *hearts;

-(instancetype)initWithHearts:(NSArray<Heart *> *)hearts;

-(void)removeLife;
-(BOOL)hasLife;
@end
