//
//  AppDelegate.h
//  Babbel
//
//  Created by A. J. on 03/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

