//
//  Heart.m
//  Babbel
//
//  Created by A. J. on 04/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Heart.h"

@implementation Heart

#pragma mark - Set

-(void)setAlive:(BOOL)alive{
    [self setImage: alive ? [UIImage imageNamed:@"heart_on"]: [UIImage imageNamed:@"heart_fade"]];
}

@end
