//
//  Heart.h
//  Babbel
//
//  Created by A. J. on 04/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Heart : UIImageView

@property(nonatomic) BOOL alive;

@end
