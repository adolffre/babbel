//
//  Cloud.h
//  Babbel
//
//  Created by A. J. on 05/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cloud : NSObject

@property (nonatomic, strong) NSString *word;
@property (nonatomic, strong) UIView *cloudView;

-(instancetype)initCloudWithSmallSize:(BOOL)smallSize andWord:(NSString *)word;

-(void)changeToRed;
-(void)changeToGreen;

@end
