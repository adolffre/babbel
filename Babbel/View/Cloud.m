//
//  Cloud.m
//  Babbel
//
//  Created by A. J. on 05/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Cloud.h"

static int const hightLabel = 40;
static int const hightCloud = 100;
static int const widthSmallCloud = 154;
static int const widthBigCloud = 307;
static int const widthSmallLabel = 80;
static int const widthBiglLabel = 180;
static int const centerYLabel = 60;
static int const fontSizeCloud = 22;

@interface Cloud()

#pragma mark - Properties

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIImageView *cloudImageView;
@property (nonatomic) BOOL isSmallCloud;

@end

@implementation Cloud

#pragma mark - Init

-(instancetype)initCloudWithSmallSize:(BOOL)smallSize andWord:(NSString *)word{
    
    self = [super init];
    if(self)
    {
        self.word = word;
        self.isSmallCloud = smallSize;
        
        if(smallSize){
            self.cloudView = [[UIView alloc] initWithFrame:
                         CGRectMake(0, 0, widthSmallCloud, hightCloud)];
            self.label =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, widthSmallLabel, hightLabel)];
            self.label.center = CGPointMake(self.cloudView.frame.size.width / 2, centerYLabel);
            
        }
        else{
            
            self.cloudView = [[UIView alloc] initWithFrame:
                         CGRectMake(0, 0, widthBigCloud, hightCloud*2)];
            self.label =[[UILabel alloc] initWithFrame:CGRectMake(0, 0, widthBiglLabel, hightLabel*2)];
            self.label.numberOfLines = 0;
            self.label.center = CGPointMake(self.cloudView.frame.size.width / 2, centerYLabel*2);
        }
        
        self.cloudImageView = [[UIImageView alloc] initWithFrame:self.cloudView.frame];
        self.cloudImageView.image = smallSize ? [self smallCloudImage] : [self bigCloudImage];
        [self.cloudView addSubview:self.cloudImageView];
        self.cloudImageView.center = CGPointMake(self.cloudView.frame.size.width / 2, self.cloudView.frame.size.height /2);
        
        [self.label setFont:[self wordTextFont]];
        self.label.textColor = [UIColor whiteColor];
        [self.label setTextAlignment:NSTextAlignmentCenter];
        self.label.text = self.word;
        [self.cloudView addSubview:self.label];
    }
    return self;
}

#pragma mark - Custom Methods

-(void)changeToRed{
    
    self.cloudImageView.image = self.isSmallCloud ? [UIImage imageNamed:@"cloud_little_red"] : [UIImage imageNamed:@"cloud_big_red"];
}

-(void)changeToGreen{
    
    self.cloudImageView.image = self.isSmallCloud ? [UIImage imageNamed:@"cloud_little_green"] : [UIImage imageNamed:@"cloud_big_green"];
}

-(UIFont *)wordTextFont {
    
    return [UIFont fontWithName:@"AvenirNext-Bold" size:fontSizeCloud];
}

#pragma mark - Getters

-(UIImage *)smallCloudImage{
    
    return [UIImage imageNamed:@"cloud_little_blue"];
}

-(UIImage *)bigCloudImage{
    
    return [UIImage imageNamed:@"cloud_big_blue"];
}

@end
