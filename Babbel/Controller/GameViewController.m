//
//  GameViewController.m
//  Babbel
//
//  Created by A. J. on 03/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "GameViewController.h"
#import "GameOverViewController.h"
#import "Clouds.h"
#import "Words.h"
#import "Heart.h"
#import "Life.h"

static float const delayClick = 0.2f;

@interface GameViewController ()<GameOverViewControllerDelegate>

#pragma mark - Propertys

@property (nonatomic, strong) NSString *fallingWord;
@property (nonatomic, strong) Clouds *clouds;
@property (nonatomic, strong) Words *words;
@property (nonatomic, strong) Life *life;
@property (nonatomic, strong) NSArray *hearts;
@property (nonatomic) NSInteger score;

#pragma mark - IBOutlets

@property (nonatomic, weak) IBOutlet UIView *finalView;
@property (nonatomic, weak) IBOutlet UILabel *scoreFinalView;
@property (nonatomic, weak) IBOutlet UILabel *word;
@property (nonatomic, weak) IBOutlet Heart *heart1;
@property (nonatomic, weak) IBOutlet Heart *heart2;
@property (nonatomic, weak) IBOutlet Heart *heart3;

@end

@implementation GameViewController

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self newGame];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"cloudOutOfView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"cloudPressed" object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:(@selector(action:))];
    
    [self.view addGestureRecognizer:tap];
    
}

#pragma mark - Start Game

-(void)newGame{

    self.words = [[Words alloc] initWords];
    self.clouds = [[Clouds alloc] initWithSuperView:self.view];
    self.life = [[Life alloc] initWithHearts:@[self.heart1, self.heart2, self.heart3]];
    
    [self loadNewWord];
    [self createNewCloud];
    
    self.score = 0;
}

#pragma mark - Words Methods

-(void)loadNewWord{
    
    [self.words createNewWord];
    self.word.text = [self.words getEnglishWord];
}

-(void)correctWord{
    
    [self.clouds stop];
    [self loadNewWord];
    [self createNewCloud];
    self.score++;
}

-(void)wrongWord{
    
    [self removeLife];
}

-(void)loadNewFallingWord{
    
    self.fallingWord = [self.words getNextFallingWord];
}

-(void)compareWords:(NSString *)word{
    
    if([[self.words getSpanishWord] isEqualToString:word]){
        [self.clouds correctWordPressed];
        [self performSelector:@selector(correctWord) withObject:nil afterDelay:delayClick];
        
    }
    else{
        [self.clouds wrongWordPressed];
        [self performSelector:@selector(wrongWord) withObject:nil afterDelay:delayClick];
    }
    
}

#pragma mark - Life Methods

-(void)removeLife{
    
    [self.life removeLife];
    
    if([self.life hasLife]){
        [self createNewCloud];
    }
    else{
        [self showGameOver];
        [self.clouds stop];
    }
}

#pragma mark - Clouds Methods

-(void)createNewCloud{
    
    [self loadNewFallingWord];
    [self.clouds showCloudWithWord:self.fallingWord];
}

#pragma mark - Actions

-(void)action:(UITapGestureRecognizer *)tap{
    
    [self.clouds fastWord];
}

-(void)showGameOver{
    
    [self performSegueWithIdentifier:@"gameOverSegueId" sender:self];
}

#pragma mark - Notification Center

-(void)receivedNotification:(NSNotification*)notification{
    
    if ([notification.name isEqualToString:@"cloudPressed"])
    {
        NSDictionary* userInfo = notification.userInfo;
        NSString *word = [userInfo objectForKey:@"word"];
        [self compareWords:word];
        
    }
    else if([notification.name isEqualToString:@"cloudOutOfView"]){
        [self createNewCloud];
    }
}

#pragma mark - GameOverViewController Delegate

-(void)playAgainPressed{
    
    [self newGame];
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"gameOverSegueId"]){
        [(GameOverViewController *)segue.destinationViewController setScore:self.score];
        [(GameOverViewController *)segue.destinationViewController setDelegate:self];
    }
}

@end
