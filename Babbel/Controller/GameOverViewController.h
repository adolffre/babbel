//
//  GameOverViewController.h
//  Babbel
//
//  Created by A. J. on 05/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GameOverViewControllerDelegate <NSObject>

-(void)playAgainPressed;

@end

@interface GameOverViewController : UIViewController

@property (nonatomic) NSInteger score;
@property (nonatomic, weak , nullable) id<GameOverViewControllerDelegate> delegate;

@end
