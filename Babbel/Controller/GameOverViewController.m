//
//  GameOverViewController.m
//  Babbel
//
//  Created by A. J. on 05/03/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "GameOverViewController.h"

@interface GameOverViewController()

#pragma mark - IBOutlets

@property (nonatomic, weak) IBOutlet UILabel *scoreLabel;

@end

@implementation GameOverViewController

#pragma mark - View Controller Lifecycle

-(void)viewDidLoad{
    
    [super viewDidLoad];
    self.scoreLabel.text = [NSString stringWithFormat:@"Your Score %li",(long)self.score];
}

#pragma mark - Actions

- (IBAction)playAgainPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate playAgainPressed];
    }];
}

@end
